import { tileLayer } from "leaflet";

export const OSMLayer = tileLayer(
  "https://api.maptiler.com/maps/streets/{z}/{x}/{y}.png?key=l6LE501kztQbg1HaKhEg",
  {
    attribution:
      '<a href="https://www.maptiler.com/copyright/" target="_blank">&copy; MapTiler</a> <a href="https://www.openstreetmap.org/copyright" target="_blank">&copy; OpenStreetMap contributors</a>',
  }
);

export const GoogleMapsLayer = tileLayer(
  "http://mt1.google.com/vt/lyrs=s&x={x}&y={y}&z={z}"
);
