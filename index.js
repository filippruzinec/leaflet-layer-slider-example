import { map as leafletMap } from "leaflet";
import "leaflet.sync";
import "leaflet/dist/leaflet.css";
import { toggleLayerSlider } from "./src/controls/layerSlider";
import { GoogleMapsLayer, OSMLayer } from "./src/layers/layers";
import "./style.css";

const map = leafletMap("map").setView([51.505, -0.09], 13);
OSMLayer.addTo(map);

const layerSliderToggle = document.getElementById("toggle-draggable");

layerSliderToggle.addEventListener("click", () => {
  toggleLayerSlider(map, GoogleMapsLayer);
});
